class parsingLogFile:
    # this function will find the frequency of the each url found in the log file and return the dictionary
    def countUrl(self,fileName):
        file = open(fileName,"r")
        urlCount={}
        for line in file.readlines():
            splitWord  = line.split(" ",12)

            if splitWord[10] not in urlCount: #to count max used url
                urlCount[splitWord[10]] = 1
            else:
                urlCount[splitWord[10]] += 1
        return urlCount

     # this function will find the frequency of the each hour found in the log file and return the dictionary
    def countHour(self,fileName):
        file = open(fileName,"r")
        hourCount={}
        for line in file.readlines():
            splitWord  = line.split(" ",12)
            datetime = splitWord[3]
            hour = datetime[-8:-6]
            if hour not in hourCount: #to count max hour
                hourCount[hour] = 1
            else:
                hourCount[hour] += 1
        return hourCount



ob = parsingLogFile()
urls = ob.countUrl("dele innoseva log.txt")                     #getting the dictionary of url and its frequency
inverse = [(value, key) for key, value in urls.items()]         #finding the max requested url
print  max(inverse)[1]  + " is the maximum requested url."
hours = ob.countHour("dele innoseva log.txt")                   #getting the dictionary of hour and its frequency
inverse = [(value, key) for key, value in hours.items()]        #finding the busiest hour of the day
print max(inverse)[1] + " is the busiest hour of the day."